# Canvas Wrapper for Eddie3

The canvas_wrapper.pl script will process family groups with Canvas, staging
the alignment files from datastore to your current directory before running
Canvas and removing the local copies of the alignment files before processing
the next family.

Canvas 1.38 is expected either in the default location on Eddie3
(/exports/igmm/eddie/IGMM-VariantAnalysis/software/Canvas-1.38.0.1554+master_x64/Canvas)
or else should be specified with the --canvas option when invoking
canvas_wrapper.pl

## Installation

Clone this repo recursively with git to ensure you get the required ped parser
module.

    git clone --recursive https://git.ecdf.ed.ac.uk/dparry/canvas_eddie3_wrapper.git

The wrapper script will be copied to canvas_eddie3_wrapper/canvas_wrapper.pl
relative to where you run the above command.

## Usage

    perl canvas_wrapper.pl [options] PED SAMPLES_TO_BAMS VCF OUTDIR

### Positional arguments (mandatory)

    PED                 A 6 column PED file for your samples of interest

    SAMPLES_TO_BAMS     A tab delimited file with sample names as the first 
                        column and location on datastore for the BAM as the
                        second column

    VCF                 VCF to use for Canvas sample-b-allele-vcf argument.
                        This should be a multisample VCF containing genotype
                        calls for all your samples in your PED.

    OUTDIR              Directory for results. Subfolders will be created for
                        each family 
                        
### Options:

    --mem MEMORY        Amount of memory per core to request. Default = 8G

    --concurrent JOBS   No. of jobs to run concurrently. Default = 1.

    --no_confirm        Do not check successful execution of previous run 

    --canvas /path/to/Canvas
                        Location of Canvas executable if not in the default
                        location (/exports/igmm/eddie/IGMM-VariantAnalysis/software/Canvas-1.38.0.1554+master_x64/Canvas)

## Note

When setting up Canvas, at the time of writing there is a bug with the program
expecting the reference files to be located in a hardcoded relative directory.
You need to create a link to the directory as per this issue: 

https://github.com/Illumina/canvas/issues/91

