#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use FindBin qw($RealBin);
use lib "$RealBin/lib/dapPerlGenomicLib";
use ParsePedfile;

my $usage = <<EOT

$0 [options] PED SAMPLES_TO_BAMS VCF OUTDIR

POSITIONAL ARGUMENTS (mandatory)

    PED                 A 6 column PED file for your samples of interest

    SAMPLES_TO_BAMS     A tab delimited file with sample names as the first 
                        column and location on datastore for the BAM as the
                        second column

    VCF                 VCF to use for Canvas sample-b-allele-vcf argument
                        This should be a multisample VCF containing genotype
                        calls for all of the samples in your PED.

    OUTDIR              Directory for results. Subfolders will be created for
                        each family 
                        
OPTIONS:

    --mem MEMORY        Amount of memory per core to request. Default = 8G

    --threads           No. cores to request for each canvas job. Default = 8.

    --concurrent JOBS   No. of jobs to run concurrently. Default = 1.

    --no_confirm        Do not check successful execution of previous run 
                        before initiating the next command. By default a queue
                        will halt upon failure of a run.

    --qsub_scripts      Use this directory for qsub scripts. Default = ./subscripts

    --do_not_submit     Do a dry run - do not submit jobs to the queue

    --canvas /path/to/Canvas
                        Location of Canvas executable if not in the default
                        location (/exports/igmm/eddie/IGMM-VariantAnalysis/software/Canvas-1.38.0.1554+master_x64/Canvas)

EOT
;

#my $canvas = "/gpfs/igmmfs01/eddie/igmm_datastore_MND-WGS/Canvas-1.38.0.1554+master_x64/Canvas";
my %opt = (mem => '8G', 
           concurrent => 1,
           threads => 8,
           qsub_scripts => 'subscripts',
           canvas => "/exports/igmm/eddie/IGMM-VariantAnalysis/software/Canvas-1.38.0.1554+master_x64/Canvas",
          );
GetOptions(
    \%opt,
    'mem=s',
    'threads=i',
    'concurrent=i',
    'no_confirm',
    'do_not_submit',
    'canvas=s',
    'qsub_scripts=s',
    'help',
) or die "Error in option spec: $!\n";

if ($opt{help}){
    print $usage;
    exit;
}

die $usage if @ARGV != 4;

my $ped = shift;
my $samp_to_bam = shift;
my $vcf = shift;
my $outdir = shift;
my %bams = ();
open (my $BAM, $samp_to_bam) or die "Could not open $samp_to_bam: $!\n";
while (my $line = <$BAM>){
    chomp $line;
    my ($samp, $bam) = split("\t", $line);
    $bams{$samp} = $bam;
}
close $BAM;

my %out_dirs = ();
my %get_scripts = ();
my %canvas_scripts = ();
my %scripts = ();
my $pedfile = ParsePedfile->new( file => $ped );
if (not -d $opt{qsub_scripts}){
    print STDERR "Creating $opt{qsub_scripts} directory\n";
    mkdir($opt{qsub_scripts}) or die "Error creating $opt{qsub_scripts} directory: $!\n";
}else{
    print STDERR "Using existing $opt{qsub_scripts} directory\n";
}
LINE: foreach my $fam ($pedfile->getAllFamilies()){
    my @aff = $pedfile->getAffectedsFromFamily($fam);
    if (not @aff){
        warn "No affecteds from family '$fam' - skipping.\n";
        next;
    }
    my $proband = undef;
    my @aff_with_par = ();
    foreach my $s (@aff){
        if (not exists $bams{$s}){
            warn "Sample '$s' not in bam list - skipping family $fam\n";
            next LINE;
        }
        #first affected with both parents will be our proband
        #otherwise affected with a parent or first affected
        my $d = $pedfile->getFather($s);
        my $m = $pedfile->getMother($s);
        if ($d and $m and exists $bams{$d} and exists $bams{$m}){
            $proband = $s;
            last;
        }elsif ($d and exists $bams{$d}){
            push @aff_with_par, $s;
        }elsif ($m and exists $bams{$m}){
            push @aff_with_par, $s;
        }
    }
    if (not defined $proband){
        if (@aff_with_par){
            $proband = $aff_with_par[0];
        }else{
            $proband = $aff[0];
        }
    }
    my %pars = map{$_ => 1} $pedfile->getParents($proband);
    my $dad = $pedfile->getFather($proband);
    my $mum = $pedfile->getMother($proband);
    $dad = ($dad and exists $bams{$dad}) ? $dad : undef;
    $mum = ($mum and exists $bams{$mum}) ? $mum : undef;
    my @other = grep { exists $bams{$_} } 
                grep { $_ ne $proband }
                grep { !$pars{$_} } $pedfile->getSamplesFromFamily($fam); 
    write_scripts($fam, $proband, $dad, $mum, \@other);
}

submit_jobs() unless $opt{do_not_submit};

#################################################
sub submit_jobs{
    if (scalar(@{$scripts{get}}) != scalar(@{$scripts{canvas}})){
        die "ERROR - number of 'get' scripts does not match number of canvas scripts - exiting!\n";
    }
    my @prev_jid = ();
    my @prev_canvas = ();
    for (my $i = 0; $i < @{$scripts{get}}; $i++){
        my $qsub = 'qsub';
        if (scalar(@prev_jid) >= $opt{concurrent}){
            $qsub .= " -hold_jid ".$prev_jid[-$opt{concurrent}];
        }
        my $cmd = "$qsub -terse $scripts{get}->[$i]";
        if (scalar(@prev_canvas) >= $opt{concurrent} and not $opt{no_confirm}){
            $cmd .= " ".$prev_canvas[-$opt{concurrent}].".stdout";
        }
        my $jid = run_command($cmd);
        push @prev_jid, run_command("qsub -hold_jid $jid -terse  $scripts{canvas}->[$i] $scripts{get}->[$i].stdout");
        push @prev_canvas, $scripts{canvas}->[$i];
    }
    print STDERR "Finished submitting jobs\n";
}
#################################################
sub write_canvas_script{
    my ($fid, $proband, $dad, $mum, $others, $res_dir) = @_;
    my $script = "$opt{qsub_scripts}/canvas_$fid.sh";
    if (exists $canvas_scripts{$fid}){
        $script = "$opt{qsub_scripts}/canvas_$fid.$canvas_scripts{$fid}.sh";
    }
    $canvas_scripts{$fid}++;
    my $threads = '';
    if ($opt{threads} > 1){
        $threads = "#\$ -pe sharedmem $opt{threads}";
    }
    my @fam_bams = ();
    my $bam_args = "--bam bams/".basename($bams{$proband})." proband $proband \\\n";
    push @fam_bams, "bams/".basename($bams{$proband});
    if ($dad){
        my $bam = "bams/".basename($bams{$dad});
        push @fam_bams, $bam;
        $bam_args .= "        --bam $bam father $dad \\\n";
    }
    if ($mum){
        my $bam = "bams/".basename($bams{$mum});
        push @fam_bams, $bam;
        $bam_args .= "        --bam $bam mother $mum \\\n";
    }
    for my $other (@$others){
        my $bam = "bams/".basename($bams{$other});
        push @fam_bams, $bam;
        $bam_args .= "        --bam $bam other $other \\\n";
    }
    my $rm_string = join(" ", map{"$_ $_.bai"} @fam_bams); 
    open (my $SCRIPT, ">", $script) or die "Error creating $script: $!\n";
    print $SCRIPT <<EOT
#!/bin/bash
#\$ -M david.parry\@igmm.ed.ac.uk
#\$ -V
#\$ -cwd
#\$ -l h_rt=48:00:00
#\$ -l h_vmem=$opt{mem}
#\$ -j y
#\$ -o $script.stdout
$threads

PREV=\$1
if [ "\$PREV" != '' ]
then
    echo \$(date) - checking previous exit status from \$PREV
    if [ "\$(tail -n 1 \$PREV)" != '0' ]
    then
        echo non-zero status from \$PREV
        exit 1
    fi
    echo \$(date) - status OK
fi

CANVAS=$opt{canvas}
export PATH=\$PATH:\$(dirname \$CANVAS)/dotnet/
REF=\$(dirname \$CANVAS)/GRCh38
VCF=$vcf

mkdir -p $res_dir

#CANVAS frequently fails and needs to restart when multithreading... - give it 5 goes
n=0
until [ \$n -ge 5 ]
do
    echo \$(date) Running canvas \\(attempt \$[\$n+1]/5\\)
    \$CANVAS SmallPedigree-WGS \\
        $bam_args        --sample-b-allele-vcf \$VCF \\
        -r \$REF/genome.fa \\
        -g \$REF \\
        -f \$REF/centromeres_and_non_standard.bed \\
        -o $res_dir && break
    n=\$[\$n+1]
    echo \$(date) Attempt \$n failed
    sleep 2
done
if [ \$n -eq 5 ]
then
    echo \$(date) No success after 5 attempts
    exit 1
else
    echo \$(date) Success after \$[\$n+1] attempts
fi

echo \$(date) Removing BAMs
rm $rm_string

echo \$?
EOT
    ;
    push @{$scripts{canvas}}, $script;
}
 
#################################################
sub write_get_script{
    my ($fid, $samp, $dad, $mum, $others) = @_;
    my $script = "$opt{qsub_scripts}/get_$fid.sh";
    if (exists $get_scripts{$fid}){
        $script = "$opt{qsub_scripts}/get_$fid.$get_scripts{$fid}.sh";
    }
    $get_scripts{$fid}++;
    my $bam_files = join(" ", map{"$bams{$_} $bams{$_}.bai"} 
                              grep {defined($_)}($samp, $dad, $mum, @$others));
    open (my $SCRIPT, ">", $script) or die "Error creating $script: $!\n";
    print $SCRIPT <<EOT
#!/bin/bash
#\$ -M david.parry\@igmm.ed.ac.uk
#\$ -V
#\$ -cwd
#\$ -l h_rt=8:00:00
#\$ -q staging
#\$ -j y
#\$ -o $script.stdout

PREV=\$1
if [ "\$PREV" != '' ]
then
    echo \$(date) - checking previous exit status from \$PREV
    if [ "\$(tail -n 1 \$PREV)" != '0' ]
    then
        echo non-zero status from \$PREV
        exit 1
    fi
    echo \$(date) - status OK
fi

set -euo pipefail
rsync -av $bam_files bams/
echo \$?
EOT
    ;
    push @{$scripts{get}}, $script;
}
    
#################################################
sub write_scripts{
    my ($fid, $samp, $dad, $mum, $others) = @_;
    my $res_dir = "$outdir/$fid";
    if (exists $out_dirs{$fid}){
        $res_dir = $res_dir . "_$out_dirs{$fid}";
    }
    $out_dirs{$fid}++;
    write_get_script($fid, $samp, $dad, $mum, $others);
    write_canvas_script($fid, $samp, $dad, $mum, $others, $res_dir);
}

##################################################
sub check_exit{
    my $e = shift;
    if($e == -1) {
        die "Failed to execute: $!\n";
    }elsif($e & 127) {
        printf "Child died with signal %d, %s coredump\n",
        ($e & 127),  ($e & 128) ? 'with' : 'without';
        die "Exiting\n";
    }elsif($e != 0){
        printf "Child exited with value %d\n", $e >> 8;
        die "Exiting\n";
    }
}

##################################################
sub run_command{
    my $cmd = shift;
    print STDERR "Executing: $cmd\n";
    my $r = `$cmd`;
    check_exit($?);
    chomp $r;
    return $r;
}
